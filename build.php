<?php
require 'config.php';
require 'utils.php';

$base_dir = "campanhas/";
$years = glob($base_dir . "*");
$paths = array();

foreach($years as $year) {
    if(is_dir($year)) {
        $months = glob($year . '/' . "*");

        foreach($months as $month) {
            if (is_dir($month)) {
                $campaigns = glob($month . '/' . "*");
                $years_name = str_replace($year, '', $month);

                foreach ($campaigns as $campaign) {
                    if (is_dir($campaign)) {
                        $paths[] = $campaign;
                    }
                }
            }
        }

    }
}
usort($paths, function ($a, $b) {
    return filemtime($a) < filemtime($b);
});


// debug($paths);

function pathName($path) {
    $path = str_replace('campanhas/', '', $path);
    $path = str_replace('/', ' - ', $path);

    return $path;
}

function debug($str) {
    echo "<pre>";
    print_r($str);
    echo "</pre>";
}

?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Lojas Pompéia - Gerar E-mail MKT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <h2>Lojas Pompéia <small>Exportação de e-mail marketing</small></h2>
            </div>

            <form role="form" class="" action="build_action.php" method="post">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="input-campaign">Campanha </label>
                        <select class="form-control" id="input-campaign" name="campaign">
                            <option value="">Selecione a campanha</option>
                            <?php foreach ($paths as $path) : ?>
                                <option value="<?php echo $path ?>"><?php echo pathName($path); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="">
                    <p>Escolha os fornecedores abaixo para gerar o HTML:</p>
                    <div class="btn-group" data-toggle="buttons">
                        <?php foreach ($sources as $source) { ?>
                            <label class="btn btn-primary">
                                <input type="checkbox" autocomplete="off" name="sources[]" value="<?php echo $source; ?>"> <?php echo $source; ?>
                            </label>
                        <?php } ?>
                    </div>
                </div>

                <button class="btn btn-default" type="submit">Enviar</button>
            </form>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script>
            // $(function () {
            //     var $sources =
            // });
        </script>
    </body>
</html>