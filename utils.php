<?php
$online = isset($_GET['abs']) ? TRUE : FALSE;
define('CONTENT_WIDTH', 650);
define('ORANGE', '#EB7123');
define('ONLINE', $online);
define('BASE_IMAGES', 'http://www.lojaspompeia.com.br/news');

function getSource() {
    $param = !isset($_GET['source']) ? FALSE : $_GET['source'];

    if (!$param) {
        return FALSE;
    }

    return $param;
}

function getImageSrc($src) {
    return getImagePath() . $src;
}

function getImagePath() {
    if (!ONLINE) {
        return 'images/';
    } else {
        $url_segments = array(BASE_IMAGES, YEAR, MONTH, CAMPAIGN, 'images');
        return implode('/', $url_segments) . '/';
    }
}

function getUrlParams($separator) {
    if (!ONLINE || !getSource()) {
        return '';
    } else {
        $source = getSource();
        $campaign_default = YEAR . MONTH . "_" . CAMPAIGN;
        $campaign = in_array($source, array('mediastay', 'springmedia')) ? 'aff_sub' : $campaign_default;
        return $separator . 'utm_source='.$source.'&utm_medium=email&utm_campaign='.$campaign;
    }
}

function printFont($size = '13px', $color = '#000000') {
    echo 'font-size: '.$size.'; color: '.$color.';';
}

function fontFamily () {
    echo 'font-family: Arial, sans-serif;';
}

function padding($top = '0', $right = '14', $bottom = '0', $left = '14') {
    echo 'padding-top: '.$top.'px; padding-right: '.$right.'px; padding-bottom: '.$bottom.'px; padding-left: '.$left.'px;';
}

function color($color = 'default', $echo = TRUE) {
    switch ($color) {
        case 'orange':
            $color = ORANGE;
            break;
        case 'default':
            $color = '#ffffff';
            break;
        default:
            $color = $color;
            break;
    }

    if ($echo)
        echo $color;
    else
        return $color;
}

function printImg($src, $alt = '', $link = FALSE, $class = '') {
    $path = getImagePath();
    $filepath = $path . $src;

    $img = '<img src="'.$filepath.'" alt="'.$alt.'" border="0" class="'.$class.'" style="display: block;">';

    if ($link) {
        echo linkElm($link, $img);
    } else {
        echo $img;
    }
}

function tableDefaultAttrs($echo = TRUE) {
    $html = 'border="0" cellpadding="0" cellspacing="0"';
    if ($echo)
        echo $html;
    else
        return $html;
}

function linkElm($link, $inside, $lineheight = '100%', $defaultcolor = '#000000', $textdecoration = 'none', $styles = '', $button = FALSE) {

    if ($link !== '#') {
        $separator = strpos($link, '?') === FALSE ? '?' : '&';

        if (strpos($link, 'facebook.com') === FALSE &&
            strpos($link, 'twitter.com') === FALSE &&
            strpos($link, 'pinterest.com') === FALSE &&
            strpos($link, 'youtube.com') === FALSE &&
            strpos($link, 'instagram.com') === FALSE) {

            $link .= getUrlParams($separator);
        }
    }

    $link = '<a href="'.$link.'" target="_blank" style="display: inline-block; font-family: Arial, sans-serif; color: '.$defaultcolor.'; line-height: '.$lineheight.'; '.$styles.' text-decoration: '.$textdecoration.';">'.$inside.'</a>';

    if ($button) {
        echo '<div style="'.$styles.' padding-top: 3px; padding-bottom: 3px;">' . $link . '</div>';
    } else {
        echo $link;
    }
}

function getFullLink ($link) {
    if ($link !== '#') {
        $separator = strpos($link, '?') === FALSE ? '?' : '&';
        $link .= getUrlParams($separator);
    }

    return $link;
}

function getLink($link) {
    if (!$link) {
        return FALSE;
    } else {
        return strpos($link, 'http') !== FALSE ? $link : 'http://www.lojaspompeia.com/' . $link;
        /*$link = getFullLink($link);

        return $link;*/
    }
}

function space($height = 25) {
    echo '<table class="container" width="'.CONTENT_WIDTH.'" '.tableDefaultAttrs(false).'><tr><td height="'.$height.'">&nbsp;</td></tr></table>';
}

?>