<?php
define('CAMPAIGN', 'must_have_inverno');
define('YEAR', '2016');
define('MONTH', '05');
define('EXPIRE_DATE', '16/05/2016');

require '../../../../../utils.php';
require '../../../../../_templates/banner.php';
require '../../../../../_templates/product.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1.0">    <!-- So that mobile webkit will display zoomed in -->
    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->

    <title>Gabriela</title>

    <?php require '../../../../../_templates/head.resetcss.php'; ?>
</head>
<body style="margin:0; padding:10px 0;" bgcolor="#ffffff">

<table width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?> bgcolor="<?php echo color(); ?>" align="center" class="wrapper">
    <tr>
        <td align="center" valign="top" bgcolor="<?php echo color(); ?>" style="<?php echo printFont(); ?>">

            <?php require '../../../../../_templates/header.php'; ?>

            <?php require '../../../../../_templates/header/menu.php'; ?>

            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?> style="padding-bottom: 0;">
                <tr>
                    <td width="100%" align="center">
                        <?php printImg('banners/banner-topo.png', "", getLink('http://www.gabriela.com.br/?utm_source=news&utm_medium=news_Gabriela&utm_campaign=institucional')) ?>
                    </td>
                </tr>
                <tr>
                    <td width="100%" align="center">
                        <?php printImg('banners/banner-meio.png', "", getLink('http://www.gabriela.com.br/outono-inverno/?utm_source=news&utm_medium=news_Gabriela&utm_campaign=outono-inverno')) ?>
                    </td>
                </tr>
            </table>

            <?php space(30); ?>


            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?> style="padding-bottom: 0;">
                <tr>
                    <td width="100%" align="center">
                        <?php printImg('banners/title-product.png', "", getLink('')) ?>
                    </td>
                </tr>
            </table>

            <?php space(30); ?>

            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td align ="center" width="216" valign="bottom">
                        <?php printImg('produtos/prod1.jpg', "", getLink('http://www.gabriela.com.br/gabriela-37320-fe-bota-md-al-md-al-000000000005060179/p?utm_source=news&utm_medium=news_Gabriela&utm_campaign=outono-inverno')) ?>
                    </td>
                    <td align ="center" width="216" valign="bottom">
                        <?php printImg('produtos/prod2.jpg', "", getLink('http://www.gabriela.com.br/bota-gabriela-franja-camurca-caramelo-000000000005062005/p?utm_source=news&utm_medium=news_Gabriela&utm_campaign=outono-inverno')) ?>
                    </td>
                    <td align ="center" width="216" valign="bottom">
                        <?php printImg('produtos/prod3.jpg', "", getLink('http://www.gabriela.com.br/bota-gabriela-cano-curto-marrom-000000000005060376/p?utm_source=news&utm_medium=news_Gabriela&utm_campaign=outono-inverno')) ?>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="" height="20" style="font-size: 0;">&nbsp;</td>
                    <td bgcolor="" height="20" style="font-size: 0;">&nbsp;</td>
                    <td bgcolor="" height="20" style="font-size: 0;">&nbsp;</td>
                </tr>
                <tr>
                    <td align ="center" width="216" valign="bottom">
                        <?php printImg('produtos/prod4.jpg', "", getLink('http://www.gabriela.com.br/bota-gabriela-over-the-knee-sintetico-preta-000000000005062003/p?utm_source=news&utm_medium=news_Gabriela&utm_campaign=outono-inverno')) ?>
                    </td>
                    <td align ="center" width="216" valign="bottom">
                        <?php printImg('produtos/prod5.jpg', "", getLink('http://www.gabriela.com.br/bota-gabriela-over-the-knee-camurca-caramelo-000000000005061999/p?utm_source=news&utm_medium=news_Gabriela&utm_campaign=outono-inverno')) ?>
                    </td>
                    <td align ="center" width="216" valign="bottom">
                        <?php printImg('produtos/prod6.jpg', "", getLink('http://www.gabriela.com.br/bota-gabriela-jeans-azul-000000000005062004/p?utm_source=news&utm_medium=news_Gabriela&utm_campaign=outono-inverno')) ?>
                    </td>
                </tr>
            </table>

            <?php space(30); ?>

            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td align="right"><?php printImg('footer/footer-01.jpg', '', 'http://www.gabriela.com.br/Masculino/Botas?utm_source=news&utm_medium=news_Gabriela&utm_campaign=outono-inverno') ?></td>
                    <td align="right"><?php printImg('footer/footer-02.jpg', '', '') ?></td>
                </tr>
            </table>

            <table bgcolor="<?php echo color ('#131313');?>" class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td width="325">
                        <table class="container" width="90%" <?php tableDefaultAttrs() ?> align="center">
                            <tr>
                                <td height="30" style="font-size: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" height="24"><?php printImg('footer/footer-social-title.png', "")?></td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table class="container" align="center" <?php tableDefaultAttrs() ?>>
                                        <tr>
                                            <td width="35" align="center"><?php printImg('footer/footer-social-fb.png', "", getLink('https://www.facebook.com/gabrielacalcados')) ?></td>
                                            <td width="35" align="center"><?php printImg('footer/footer-social-yt.png', "", getLink('https://www.youtube.com/channel/UCLP0iB08nLWtrthiaeABo0A')) ?></td>
                                            <td width="35" align="center"><?php printImg('footer/footer-social-it.png', "", getLink('https://www.instagram.com/gabrielacalcados/')) ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="10" style="font-size: 0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td width="325">
                        <table class="container" width="90%" <?php tableDefaultAttrs() ?> align="center">
                            <tr>
                                <td height="40" style="font-size: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" height="24"><?php printImg('footer/footer-callcenter-title.png', "")?></td>
                            </tr>
                            <tr>
                                <td height="24" valign="middle" align="center">
                                    <table class="container" align="center" <?php tableDefaultAttrs() ?> width="260">
                                        <tr>
                                            <td align="left"><?php printImg('footer/footer-callcenter-phone.png', "") ?></td>
                                            <td align="right"><?php printImg('footer/footer-callcenter-mail.png', "", getLink('')) ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="10" style="font-size: 0;">&nbsp;</td>
                            </tr>
                        </table>   
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="85%" <?php tableDefaultAttrs() ?>>
                            <tr>
                                <td bgcolor="#585858" height="1" style="font-size: 0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td align="center">
                        <table width="85%" <?php tableDefaultAttrs() ?>>
                            <tr>
                                <td bgcolor="#585858" height="1" style="font-size: 0;">&nbsp;</td>
                            </tr>
                        </table>                        
                    </td>
                </tr>
            </table>

            <table bgcolor="<?php echo color ('#131313');?>" class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td width="325">
                        <table class="container" width="50%" <?php tableDefaultAttrs() ?> align="center">
                            <tr>
                                <td height="22" style="font-size: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle"><?php printImg('footer/footer-payment-title.png', "")?></td>
                            </tr>
                            <tr>
                                <td height="50" valign="middle">
                                    <table class="container" align="center" <?php tableDefaultAttrs() ?>>
                                        <tr>
                                            <td><?php printImg('footer/footer-payment-icons.png', "", getLink('')) ?></td>
                                        </tr>
                                    </table>
                                    <tr>
                                        <td bgcolor="#171717" height="1" style="font-size: 0;">&nbsp;</td>
                                    </tr>
                                </td>
                            </tr>
                        </table>    
                    </td>
                    <td width="325">
                        <table class="container" width="50%" <?php tableDefaultAttrs() ?> align="center">
                            <tr>
                                <td height="22" style="font-size: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle"><?php printImg('footer/footer-security-title.png', "")?></td>
                            </tr>
                            <tr>
                                <td height="40" valign="middle">
                                    <table class="container" align="center" <?php tableDefaultAttrs() ?>>
                                        <tr>
                                            <td><?php printImg('footer/footer-security-icons.png', "", getLink('')) ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>   
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="85%" <?php tableDefaultAttrs() ?>>
                            <tr>
                                <td bgcolor="#585858" height="1" style="font-size: 0;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td align="center">
                        <table width="85%" <?php tableDefaultAttrs() ?>>
                            <tr>
                                <td bgcolor="#585858" height="1" style="font-size: 0;">&nbsp;</td>
                            </tr>
                        </table>                        
                    </td>
                </tr>
            </table>
            <table bgcolor="<?php echo color ('#131313');?>" class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td height="50" align="center" valign="bottom"><?php printImg('footer/gabriela-logo.png', '', 'http://www.gabriela.com.br/') ?></td>
                </tr>
                <tr>
                    <td valign="middle" height="110">
                        <table width="80%" align="center" <?php tableDefaultAttrs() ?>>
                            <tr>
                                <td style="color: #fff; font-family: Arial; font-size: 9px; text-align: center; line-height: 150%;">&copy;Copyright – 2015-2016 - Todos os direitos reservados. A Loja de Cal&ccedil;ados Gabriela reserva-se no direito de corrigir ou alterar informa&ccedil;&otilde;es como: pre&ccedil;os, promo&ccedil;&otilde;es, valor de frete de entrega e disponibilidade de estoque a qualquer momento. Em caso de d&uacute;vida entre em contato conosco pelo fone (48) 3298-6999, ou atrav&eacute;s do e-mail falecom@gabriela.com.br. Raz&atilde;o Social: Calcenter Cal&ccedil;ados Centro Oeste Ltda. CNPJ: 15.048.754/0118-08– IE: 257.7187-61. Rua Roney Henrique Heiderscheidt, s/n, quadra D/E, Sala B, Bairro: Jardim Eldorado, CEP: 88133-515 Palho&ccedil;a - SC. Acesse ou baixe aqui o C&oacute;digo de Defesa do Consumidor.</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="40">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
