            <!-- MARCAS PR&Oacute;PRIAS -->
            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td bgcolor="<?php echo color(); ?>" style="<?php fontFamily(); ?> <?php echo printFont('18px', color('orange', false)); ?> padding-bottom: 10px; text-align: center; font-weight: bold;" width="100%" height="80" class="marcas-title">MARCAS PR&Oacute;PRIAS</td>
                </tr>
                <tr>
                    <td bgcolor="<?php echo color(); ?>" style="padding-bottom: 44px;" class="marcas-item-container">
                        <table <?php tableDefaultAttrs() ?> width="575" align="center" class="fullmobile">
                            <tr>
                                <td align="center" height="100" class="marcas-item">
                                    <?php printImg('marcas-proprias/eletron.png', 'El&eacute;tron', 'http://www.lojaspompeia.com/Eletron') ?>
                                </td>
                                <td align="center" class="marcas-item" style="border-left: 1px solid #C6C6C6; border-right: 1px solid #C6C6C6;" height="100">
                                    <?php printImg('marcas-proprias/vels.png', 'Vels', 'http://www.lojaspompeia.com/Vels') ?>
                                </td>
                                <td align="center" height="100" class="marcas-item">
                                    <?php printImg('marcas-proprias/autentique.png', 'Autentique', 'http://www.lojaspompeia.com/Autentique') ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>