            <!-- DIFERENCIAIS -->
            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td bgcolor="<?php echo color('orange'); ?>" style="<?php echo printFont(); ?>" width="100%">

                        <table <?php tableDefaultAttrs() ?> width="286" align="left" class="col-2-1">
                            <tr>
                                <td align="center" width="50%" height="62" class="dif-item">
                                    <?php printImg('diferenciais/5x-sem-juros.png', '5x sem juros nos cartoes*') ?>
                                </td>
                                <td align="center" width="50%" height="62" class="dif-item">
                                    <?php printImg('diferenciais/mais-de-5-mil-produtos.png', '+ de 5.000 produtos') ?>
                                </td>
                            </tr>
                        </table>

                        <table <?php tableDefaultAttrs() ?> width="286" align="right" class="col-2-1">
                            <tr>
                                <td align="center" width="50%" height="62" class="dif-item">
                                    <?php printImg('diferenciais/troca-gratuita.png', '1º troca gratis* At&eacute; 7 dias') ?>
                                </td>
                                <td align="center" width="50%" height="62" class="dif-item">
                                    <?php printImg('diferenciais/73-lojas-fisicas.png', '73 lojas fisicas') ?>
                                </td>
                            </tr>
                        </table>

                    </td>

                </tr>
            </table>