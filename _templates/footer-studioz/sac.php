            <!-- SAC -->
            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td bgcolor="<?php color('#E7490F') ?>" style="padding-top: 12px; padding-bottom: 12px;">
                        <table <?php tableDefaultAttrs() ?> align="left" width="300" class="fullmobile">
                            <tr>
                                <td height="94" width="103" align="right"><?php printImg('icon-talk.png', '') ?></td>
                                <td height="94" width="197" align="left" style="<?php fontFamily(); ?> <?php echo printFont('11px', '#ffffff'); ?> padding-left: 10px; border-right: 1px solid #B93B0D; color: <?php color() ?>;">
                                    <b>Atendimento</b><br>
                                    Segunda &agrave; sexta: das 8h &agrave;s 18h<br>
                                    S&aacute;bado: das 8h &agrave;s 14h
                                </td>
                            </tr>
                        </table>
                        <table <?php tableDefaultAttrs() ?> align="left" width="275" class="fullmobile">
                            <tr>
                                <td height="94" width="103" align="right"><?php printImg('icon-phone.png', '') ?></td>
                                <td height="94" width="197" align="left" style="<?php fontFamily(); ?> <?php echo printFont('11px', '#ffffff'); ?> padding-left: 10px; color: <?php color() ?>;">
                                    <b>Compre pelo telefone</b><br>
                                    <span style="font-size: 18px; font-weight: bold;">51. 3003-4009</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>