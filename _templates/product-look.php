<?php

/**
 * Snippet do html de cada produto
 * @param  String  $link      Link do produto
 * @param  String  $img       Nome da imagem localizada dentro da pasta de "produtos"
 * @param  String  $name      Nome do produto (utilizado também no alt da imagem)
 * @param  String $priceFrom  Preço original do produto (se o valor for FALSE, omite a informação)
 * @param  String  $priceTo   Preço atual do produto
 * @param  String  $plots     Informação de parcelamento do produto
 * @param  String $info       Informação adicional que fica abaixo do parcelamento
 * @return HTML
 */
function productLook($link, $img, $name, $priceFrom = FAlSE, $priceTo, $plots, $info = FALSE) {
    $priceFromStyle = $priceFrom ? 'text-decoration: line-through;' : '';

    if ($priceFrom !== FALSE)
        $priceFrom = $priceFrom ? "De: R$ " . $priceFrom : "&nbsp;";

    $priceTo = $priceTo;
    $info = $info ? $info : "no cart&atilde;o de cr&eacute;dito";
    $plots = $plots ? "ou " . $plots : FALSE;
    $alt = str_replace('/', '-', $name);

?>

                        <!-- # PRODUTO -->
                        <table <?php tableDefaultAttrs() ?> width="195" align="left" class="product">
                            <tr>
                                <td align="left" class="product-td" height="552" width="195" style="padding-top: 25px;">
                                    <div style="padding-left: 15px;">
                                        <table <?php tableDefaultAttrs() ?> width="180" align="center">
                                            <tr>
                                                <td class="product-image">
                                                    <?php printImg('produtos/'.$img, $alt, getLink($link)) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="product-name" valign="top" height="25" style="<?php fontFamily(); ?> font-weight: bold; padding-top: 15px; padding-bottom: 10px; font-size: 14px;"><?php echo $name; ?></td>
                                            </tr>
                                            <?php if ($priceFrom !== FALSE) : ?>
                                            <tr>
                                                <td class="product-price-from" style="<?php fontFamily(); ?> <?php echo printFont('12px', '#777777') ?> <?php echo $priceFromStyle; ?>"><?php echo $priceFrom; ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <tr>
                                                <td class="product-price-info" style="<?php fontFamily(); ?> <?php echo printFont('11px', '#919191') ?> padding-bottom: 3px;">Por:</td>
                                            </tr>
                                            <tr>
                                                <td class="product-price-to" style="<?php fontFamily(); ?> <?php echo printFont('13px', '#f7941f') ?> font-weight: bold; padding-bottom: 3px;"><?php echo $priceTo; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="product-price-info" style="<?php fontFamily(); ?> <?php echo printFont('11px', '#919191') ?> padding-bottom: 15px;">sem juros</td>
                                            </tr>
                                            <?php if ($plots !== FALSE) : ?>
                                            <tr>
                                                <td class="product-price-instalments" style="<?php fontFamily(); ?> color: <?php color('orange') ?>; font-size: 13px;"><?php echo $plots ?></td>
                                            </tr>
                                            <tr>
                                                <td class="product-price-info" style="<?php fontFamily(); ?> <?php echo printFont('11px', '#919191') ?> padding-bottom: 15px;"><?php echo $info; ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <tr>
                                                <td class="product-buy" bgcolor="<?php color ('orange') ?>" height="51"><?php linkElm(getLink($link), 'COMPRAR', '45px', '#ffffff', 'none', 'text-align: center; background-color: ' . color('orange', false) . '; font-size: 15px; font-weight: bold; width: 180px;', TRUE) ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!-- # END PRODUTO -->

<?php } ?>