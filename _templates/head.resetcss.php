<style type="text/css">
    /* RESET */
    html {width: 100%;} body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; -webkit-font-smoothing: antialiased; word-wrap:normal;}
    /* Hotmail */
    .ReadMsgBody, .ExternalClass {width:100%; display:block !important;} .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
    h1, h2, h3, h4, h5, h6 {color: black !important;} h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
    h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active { color: red !important; } h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited { color: purple !important; }
    /* Outlook */
    table td {border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;} #outlook a {padding:0;} #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;} p.MsoNormal {margin: 0px}
    /* Yahoo */
    p {margin: 1em 0;}
    /* Images */
    img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;} a img {border:none;} .image_fix {display:block;}
    /* Override phone number link styling */
    a[href^="tel"], a[href^="sms"] { text-decoration: none; color: black; pointer-events: none; cursor: default; }
    /* /RESET */

</style>