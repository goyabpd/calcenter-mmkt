            <!-- INFORMA&Ccedil;&Atilde;O COPYRIGHT -->
            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td bgcolor="<?php echo color('#ffffff'); ?>" width="100%" height="80">
                        <table <?php tableDefaultAttrs() ?> align="center" width="80%">
                            <tr>
                                <td style="<?php fontFamily(); ?> text-align: center; <?php echo printFont('8px', '#a6a6a6'); ?>">
                                    &copy; Copyright 2015-2016 - Todos os direitos reservados. A Loja de Calçados Gabriela reserva-se no direito de corrigir ou alterar informações como: preços, promoções, valor de frete de entrega e disponibilidade de estoque a qualquer momento. Em caso de dúvida entre em contato conosco pelo fone (48) 3298-6999, ou através do e-mail falecom@gabriela.com.br. Razão Social: Calcenter Calçados Centro Oeste Ltda. CNPJ: 15.048.754/0118-08– IE: 257.7187-61. Rua Roney Henrique Heiderscheidt, s/n, quadra D/E, Sala B, Bairro: Jardim Eldorado, CEP: 88133-515 Palhoça - SC. Acesse ou baixe aqui o Código de Defesa do Consumidor.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>