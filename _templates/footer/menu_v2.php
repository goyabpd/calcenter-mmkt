<?php

    function menuItem($name, $link) {

 ?>

                                        <tr>
                                            <td height="22" valign="middle"><?php linkElm('http://www.lojaspompeia.com/'.$link, $name, '22px', '#777777', 'none', 'font-size: 13px;') ?></td>
                                        </tr>

<?php } ?>

            <!-- MENU MODA MASCULINA -->
            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td bgcolor="<?php echo color('#505050'); ?>" style="<?php fontFamily(); ?> <?php echo printFont('12px', '#ffffff'); ?> text-align: center; font-weight: bold" width="100%" height="30">Moda Masculina</td>
                </tr>
                <tr>
                    <td bgcolor="<?php color('#ffffff') ?>" style="<?php padding('15', '0', '22', '25') ?>">
                        <table align="left" width="275" class="submenu-column-container">
                            <tr>
                                <td style="<?php fontFamily(); ?> <?php printFont('12px', color('orange', false)) ?> font-weight: bold;" height="30">Roupas</td>
                            </tr>
                            <tr>
                                <td>
                                    <table <?php tableDefaultAttrs() ?> align="left" width="123">
                                        <?php
                                            $items = array(
                                                'Agasalhos'         =>  'masculino/agasalhos',
                                                'Bermudas'          =>  'masculino/bermudas',
                                                'Cal&ccedil;as'     =>  'masculino/calcas',
                                                'Camisas'           =>  'masculino/camisas',
                                                'Camisetas'         =>  'masculino/camisetas'
                                            );

                                            foreach($items as $name => $link) {
                                                menuItem($name, $link);
                                            }
                                        ?>
                                    </table>
                                    <table <?php tableDefaultAttrs() ?> align="left" width="123">
                                        <?php
                                            $items = array(
                                                'Casacos'       =>  'masculino/casacos-e-jaquetas',
                                                'Moletons'      =>  'masculino/moletons',
                                                'P&oacute;los'  =>  'masculino/polos',
                                                'Pijamas'       =>  'masculino/pijamas'
                                            );

                                            foreach($items as $name => $link) {
                                                menuItem($name, $link);
                                            }
                                        ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table align="left" width="275" class="submenu-column-container">
                            <tr>
                                <td style="<?php fontFamily(); ?> <?php printFont('13px', color('orange', false)) ?> font-weight: bold;" height="30">Cal&ccedil;ados Masculinos</td>
                            </tr>
                            <tr>
                                <td>
                                    <table <?php tableDefaultAttrs() ?> align="left" width="123">
                                        <?php
                                            $items = array(
                                                'Botas'             =>  'masculino/calcados/botas',
                                                'Chinelos'          =>  'masculino/calcados/chinelos',
                                                'Sapatos'           =>  'masculino/calcados/sapato',
                                                'Sapat&ecirc;nis'   =>  'masculino/calcados/sapatenis',
                                                'Sandalias'         =>  'masculino/calcados/sandalias-e-papetes'
                                            );

                                            foreach($items as $name => $link) {
                                                menuItem($name, $link);
                                            }
                                        ?>
                                    </table>
                                    <table <?php tableDefaultAttrs() ?> align="left" width="123">
                                        <?php
                                            $items = array(
                                                'T&ecirc;nis Casual'        =>  'masculino/calcados/tenis-casual',
                                                'T&ecirc;nis Esportivo'     =>  'masculino/calcados/tenis-esportivo',
                                                'T&ecirc;nis Adventure'     =>  'masculino/calcados/tenis-adventure'
                                            );

                                            foreach($items as $name => $link) {
                                                menuItem($name, $link);
                                            }
                                        ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>