            <!-- REDES SOCIAIS -->
            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td bgcolor="<?php echo color('#ffffff'); ?>" style="<?php printFont('14px', '#ffffff'); ?>" width="100%">
                        <table <?php tableDefaultAttrs() ?> width="100%" align="center" class="fullmobile">
                            <tr>
                                <td bgcolor="<?php echo color(); ?>" height="40" align="center" ><?php printImg('social-title.jpg') ?></td>
                            </tr>
                        </table>
                        <table <?php tableDefaultAttrs() ?> width="300" align="center" class="fullmobile">
                            <tr>
                                <td height="57">
                                    <table <?php tableDefaultAttrs() ?> width="95%" align="center" class="footer-icons-container">
                                        <tr>
                                            <td align="center"><?php printImg('footer/icon-fb.jpg', '', 'https://www.facebook.com/gabrielacalcados') ?></td>
                                            <td align="center"><?php printImg('footer/icon-it.jpg', '', 'https://www.instagram.com/gabrielacalcados/') ?></td>
                                            <td align="center"><?php printImg('footer/icon-yt.jpg', '', 'https://www.youtube.com/channel/UCLP0iB08nLWtrthiaeABo0A') ?></td>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>