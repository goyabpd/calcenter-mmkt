<?php

function banner($img, $link, $alt = '') {
    $img = $img = 'banners/' . $img;
?>

            <table class="container" width="<?php echo CONTENT_WIDTH?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td style="<?php echo printFont(); ?>" width="100%">
                        <?php printImg($img, $alt, getLink($link), 'fluidimg') ?>
                    </td>

                </tr>
            </table>

<?php } ?>