<?php

/**
 * Snippet do html de cada produto
 * @param  String  $link      Link do produto
 * @param  String  $img       Nome da imagem localizada dentro da pasta de "produtos"
 * @param  String  $name      Nome do produto (utilizado tamb&eacute;m no alt da imagem)
 * @param  String $priceFrom  Pre&ccedil;o original do produto (se o valor for FALSE, omite a informa&ccedil;&atilde;o)
 * @param  String  $priceTo   Pre&ccedil;o atual do produto
 * @param  String  $plots_price     Informa&ccedil;&atilde;o de parcelamento do produto
 * @param  String $info       Informa&ccedil;&atilde;o adicional que fica abaixo do parcelamento
 * @return HTML
 */
function product($link, $img, $name, $priceFrom = FAlSE, $priceTo, $plots_price, $plots = '5', $priceFull) {
    $priceFromStyle = $priceFrom ? 'text-decoration: line-through;' : '';

    if ($priceFrom !== FALSE)
        $priceFrom = $priceFrom ? "De: R$ " . $priceFrom : "&nbsp;";

    //$priceTo = "Por R$ " . $priceTo;
    //$info = "sem juros no cart&atilde;o de cr&eacute;dito";
    //$plots_price = strpos($plots_price, 'vista') !== FALSE ? $plots_price : $plots . 'x de R$ ' . $plots_price;
    $plots_price = FALSE ? $plots_price : $plots . ' ' . $plots_price;
    $alt = str_replace('/', '-', $name);
    $priceFull = " - " . $priceFull;
    //$priceFull = "&Agrave; VISTA: R$ " . $priceFull;

    $paddingBottomPriceTo = !$plots_price ? '45px' : '3px';
    // $colorPriceTo = !$plots_price ? ORANGE : '#000000'; // changed for mmkt "12/2014 - amigo-oculto"
    $colorPriceTo = '#000000'; // changed for mmkt "12/2014 - amigo-oculto"

?>

                        <!-- # PRODUTO -->
                        <!-- <table <?php tableDefaultAttrs() ?> width="195" align="left" class="product">
                            <tr> -->
                                <td align="center" class="product-td" width="200" style="padding-top: 30px;">
                                    <div>
                                        <table <?php tableDefaultAttrs() ?> width="180" align="center">
                                            <tr>
                                                <td class="product-name" valign="top" height="45" style="<?php fontFamily(); ?> font-weight: bold; text-align: center; padding-top: 5px; padding-bottom: 10px; font-size: 13px;"><?php echo $name; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="product-image">
                                                    <?php printImg('produtos/'.$img, $alt, getLink($link)) ?>
                                                </td>
                                            </tr>
                                            <?php if ($plots_price !== FALSE) : ?>
                                            <tr>
                                                <td class="product-price-instalments" style="<?php fontFamily(); ?> padding-top: 25px; font-size: 20px; text-align: center;"><?php echo $plots_price ?></td>
                                            </tr>

                                            <tr>
                                                <td class="product-price-info" style="<?php fontFamily(); ?> <?php echo printFont('14px', '#919191') ?> padding-bottom: 10px; text-align: center;"><?php echo $priceFull ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <tr>
                                                <td class="product-buy" bgcolor="<?php color ('#f18800') ?>" height="15"><?php linkElm(getLink($link), 'COMPRE', '15px', '#ffffff', 'none', 'text-align: center; background-color: ' . color('f18800', false) . '; font-size: 12px; font-weight: bold; width: 180px;', TRUE) ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                           <!--  </tr>
                        </table> -->
                        <!-- # END PRODUTO -->

<?php } ?>