<?php

function look($img, $link, $products, $lookName, $lookDescription, $colors = array()) {
    $img = 'looks/' . $img;
    $bgDescription = array_key_exists('bg_description', $colors) ? $colors['bg_description'] : '#7f0715';
    $bgImage = array_key_exists('bg_image', $colors) ? $colors['bg_image'] : '#172546';
    $priceColor = array_key_exists('color_price', $colors) ? $colors['color_price'] : '#c71025';
    $nameColor = array_key_exists('color_name', $colors) ? $colors['color_name'] : '#ffffff';
    $bgButton = array_key_exists('bg_button', $colors) ? $colors['bg_button'] : '#f7941d';
    $buttonColor = array_key_exists('color_button', $colors) ? $colors['color_button'] : '#ffffff';
    $textColor = array_key_exists('color_text', $colors) ? $colors['color_text'] : '#ffffff';

    $tdProductPadding = sizeof($products) < 5 ? "padding-top: 15px; padding-bottom: 22px;" : "padding-top: 10px; padding-bottom: 13px;";
    $tdMiddleProductPadding = "padding-top: 65px; padding-bottom: 82px;";

    $border = array_key_exists('border', $colors) ? 'border-left: 1px solid '.$colors['border'].'; border-top: 1px solid '.$colors['border'].'; border-right: 1px solid '.$colors['border'].'; border-bottom: 1px solid '.$colors['border'].';' : '';

?>

            <!-- LOOK -->
            <table class="container" width="<?php echo CONTENT_WIDTH ?>" <?php tableDefaultAttrs() ?>>
                <tr>
                    <td style="<?php echo printFont(); ?>" width="100%" bgcolor="<?php echo color($bgDescription) ?>">

                        <table <?php tableDefaultAttrs() ?> width="385" align="left" class="fullmobile" style="<?php echo $border; ?>">
                            <tr>
                                <td bgcolor="<?php echo color($bgImage); ?>" width="226">
                                    <?php printImg($img, 'LOOK' . $lookName) ?>
                                </td>
                                <td bgcolor="<?php echo color($bgImage); ?>" style="text-align: left;">
                                    <table <?php tableDefaultAttrs() ?>>
                                        <?php
                                            foreach ($products as $i=>$product) {
                                                $textPlots = array_key_exists('plots', $product) ? ' ou ' . $product['plots'] : '';
                                        ?>
                                        <tr>
                                            <td style="<?php echo (sizeof($products) === 3 && $i === 1) ? $tdMiddleProductPadding : $tdProductPadding; ?>" width="162">
                                                <table <?php tableDefaultAttrs() ?>>
                                                    <tr>
                                                        <td style="<?php fontFamily(); ?> font-size: 12px; font-weight: bold; color: <?php echo $textColor; ?>;"><?php echo $product['name']; ?></td>
                                                    </tr>
                                                    <?php if (array_key_exists('priceFrom', $product)) {?>
                                                    <tr>
                                                        <td style="<?php fontFamily(); ?> color: <?php echo $textColor; ?>; font-size: 11px; text-decoration: line-through;">de R$ <?php echo $product['priceFrom']; ?> por</td>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td style="color: <?php color($priceColor); ?>; font-weight: bold; font-size: 16px;"><span style="<?php fontFamily(); ?> font-size: 16px;">R$</span> <span style="<?php fontFamily(); ?> font-size: 30px;"><?php echo $product['priceBig'] ?></span><span style="<?php fontFamily(); ?> font-size: 16px;">,<?php echo $product['priceSmall'] ?></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="<?php fontFamily(); ?> color: <?php echo $textColor; ?>; font-size: 11px;">&agrave; vista<?php echo $textPlots; ?></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php } ?>

                                    </table>
                                </td>
                            </tr>
                        </table>

                        <table <?php tableDefaultAttrs() ?> width="190" align="right" class="fullmobile">
                            <tr>
                                <td align="center" style="padding-top: 46px;" class="look-td">
                                    <table <?php tableDefaultAttrs() ?> width="160" class="look-table" align="left">
                                        <tr>
                                            <td style="<?php fontFamily(); ?> <?php printFont('18px', $nameColor) ?> font-weight: bold; padding-bottom: 15px;"><?php echo $lookName ?></td>
                                        </tr>
                                        <tr>
                                            <td class="look-description" style="<?php fontFamily(); ?> font-size: 13px; color: <?php color() ?>; padding-bottom: 110px;">
                                                <?php echo $lookDescription; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="<?php color($bgButton) ?>" height="51" class="look-link">
                                                <?php linkElm(getLink($link), 'APROVEITE OS LOOKS', '45px', $buttonColor, 'none', 'text-align: center; background-color: ' . color($bgButton, false) . '; font-size: 12px; font-weight: bold; width: 160px;', TRUE) ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td>

                </tr>
            </table>
<?php } ?>